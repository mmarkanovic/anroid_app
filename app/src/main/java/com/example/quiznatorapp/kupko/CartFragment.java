package com.example.quiznatorapp.kupko;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by User on 2/28/2017.
 */

public class CartFragment extends Fragment implements NameClickListener{
    private static final String TAG = "CartFragment";
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private Button btBuy;
    private EditText sum;
    private Realm realm;
    private Button btShipp;
    int sumPrice=0;
    Item item;
    List<Item> items;
    public List<ShippingInfo> ships;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_fragment,container,false);
        final FragmentActivity c = getActivity();
        btBuy = (Button) view.findViewById(R.id.btBuy);


        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(c));
        recyclerAdapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);
        sum=view.findViewById(R.id.sum);
        try{
            Realm.init(view.getContext());
        }catch (Exception e){

        }   try{
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            RealmResults<Item> realmResults=realm.where(Item.class).findAll();
            items=realm.copyFromRealm(realmResults);
            realm.close();
            recyclerAdapter.addData(items);
        }catch (Exception e){

        }

        for(Item i:items) {
            sumPrice += Integer.parseInt(i.getPrice( ));
        }
            sum.setText(String.valueOf(sumPrice));
        btBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("Orders");


                    try{
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        RealmResults<ShippingInfo> realmResult=realm.where(ShippingInfo.class).equalTo("Id",1).findAll();
                        ships=realm.copyFromRealm(realmResult);
                        realm.close();
                        order order=new order(items, ships.get(ships.size()-1 ));
                        Gson gson=new GsonBuilder().create();
                        String json=gson.toJson(order);
                        myRef.push().setValue(json);
                        Toast.makeText(getActivity(), "Zahvaljujemo se na narudžbi",Toast.LENGTH_SHORT).show();
                    }catch (Exception e){

                    }

                }
                catch(Exception e){
                Toast.makeText(getActivity(), e.toString(),Toast.LENGTH_SHORT).show();
            }
            }

        });


        return view;
    }
    public void onNameClick(int position) {
        recyclerAdapter.removeCell(position);
        //items.remove(position);
        try{
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            RealmResults<Item> result = realm.where(Item.class).equalTo("Id",items.get(position).getId()).findAll();
            result.deleteAllFromRealm();
            realm.commitTransaction();
            realm.close();
            sumPrice-=Integer.parseInt(items.get(position).getPrice());
            sum.setText(String.valueOf(sumPrice));
        }catch(Exception e){
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show( );
        }
    }

}
