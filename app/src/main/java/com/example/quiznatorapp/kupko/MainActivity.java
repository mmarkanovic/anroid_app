package com.example.quiznatorapp.kupko;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity implements NameClickListener{
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private Button btnAdd;
    private Realm realm;
    private Button btnSearch;
    private Button kosarica;
    private EditText etName;
    List<Item> Items;
    List<Item> Cart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecycler();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("item");
        Items= new ArrayList <Item>();
        Cart=new ArrayList <Item>();
        kosarica=findViewById(R.id.kosarica);
        btnAdd = findViewById(R.id.btnAdd);
        btnSearch = findViewById(R.id.btnSearch);
        etName = findViewById(R.id.etName);
        List<String> data=new ArrayList <>();
        try{
            Realm.init(getApplicationContext());
        }catch (Exception e){
            etName.setText(e.toString());
        }

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String fullName = ds.child("fullName").getValue(String.class);
                    String notes = ds.child("notes").getValue(String.class);
                    String price = ds.child("price").getValue(String.class);
                    String url = ds.child("url").getValue(String.class);
                    String quantity= ds.child("quantity").getValue(String.class);
                    Item item=new Item(fullName,notes,price,quantity,url);
                    Items.add(item);
                }
                recyclerAdapter.addData(Items);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                List<Item> searchItem=new ArrayList<Item>( );
                String search=etName.getText().toString();
                for(Item item:Items){
                    if(item.getFullname().toLowerCase().contains(search.toLowerCase())){
                        searchItem.add(item);
                    }
                }
                recyclerAdapter.addData(searchItem);
            }
        });
        kosarica.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                try{
                    Intent myIntent = new Intent(MainActivity.this, CartActivity.class);
                    myIntent.putExtra("Cart",(Serializable) Cart);
                    startActivity(myIntent);
                }catch (Exception e){
                    etName.setText(e.toString());
                }

            }
        });

    }


    private void setupRecycler(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void onNameClick(int position) {
        addToChart(position);

    }

    private void addToChart(int position) {
        Cart.add(Items.get(position));
        try{

            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Items.get(position).setId(new Date().getTime());
            Item realItem=realm.copyToRealmOrUpdate(Items.get(position));
            realm.commitTransaction();
            realm.close();
        } catch (Exception e){
            etName.setText(e.toString());
        }
    }


}
