package com.example.quiznatorapp.kupko;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by User on 2/28/2017.
 */

public class ShippingLayout extends Fragment {
    private static final String TAG = "ShippingLayout";

    private Button btShipp;
    ShippingInfo user;
    EditText Name;
    EditText Surname;
    EditText Adress;
    EditText Phone;
    Realm realm;
    public List<ShippingInfo> ships;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shipping_fragment,container,false);
        btShipp = (Button) view.findViewById(R.id.btnShippment);
        Name= view.findViewById(R.id.Name);
        Surname= view.findViewById(R.id.Surname);
        Adress= view.findViewById(R.id.Adress);
        Phone= view.findViewById(R.id.Phone);
        Name.setText("");
        try{
            Realm.init(view.getContext());
        }catch (Exception e){
        }
        try{
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            RealmResults<ShippingInfo> realmResult=realm.where(ShippingInfo.class).equalTo("Id",1).findAll();
            ships=realm.copyFromRealm(realmResult);
            realm.close();
            Name.setText(ships.get(ships.size()-1).getName());
            Surname.setText(ships.get(ships.size()-1).getSurname());
            Adress.setText(ships.get(ships.size()-1).getAdress());
            Phone.setText(ships.get(ships.size()-1).getPhone());
        }catch (Exception e){
        }

        btShipp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 ShippingInfo user = new ShippingInfo(Name.getText( ).toString( ), Surname.getText( ).toString( ), Adress.getText( ).toString( ), Phone.getText( ).toString( ), 1);
                ships.add(user);
                Name.setText(ships.get(0).getName().toString());
                try {
                    realm = Realm.getDefaultInstance();
                    realm.beginTransaction( );
                    ShippingInfo realItem = realm.copyToRealmOrUpdate(ships.get(0));
                    realm.commitTransaction( );
                    realm.close( );
                }
                catch (Exception e){
                    Surname.setText(e.toString());
                }

            }
        });


        return view;
    }
}
