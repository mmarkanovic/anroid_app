package com.example.quiznatorapp.kupko;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<NameViewHolder> {

    private List<Item> dataList = new ArrayList<Item>();
    private NameClickListener clickListener;
    String context;
    public RecyclerAdapter(NameClickListener listener) {clickListener=listener;}

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View cellView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_name, viewGroup, false);
        return new NameViewHolder(cellView, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder nameViewHolder, int position) {
        String id;
            id="";
            id+=dataList.get(position).getFullname()+"\n";
            id+=dataList.get(position).getNotes()+"\n";
            id+="Cijena: "+dataList.get(position).getPrice();

        nameViewHolder.setName(id,dataList.get(position).getUrl());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addData(List<Item> data){
        dataList.clear();
        dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewCell(Item name, int position){
        if (position >= 0 && position <= dataList.size()){
            dataList.add(position, name);
            notifyItemInserted(position);
        }
    }

    public void removeCell(int position){
        if (position >= 0 && position < dataList.size()){
            dataList.remove(position);
            notifyItemRemoved(position);
        }
    }
}
