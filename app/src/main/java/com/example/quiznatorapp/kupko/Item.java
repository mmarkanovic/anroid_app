package com.example.quiznatorapp.kupko;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Item extends RealmObject implements Serializable {
    @PrimaryKey
    private long Id;
    private String Fullname;
    private String Notes;
    private String Price;
    private String Quantity;
    private String url;

    public Item(String fullname, String notes, String price, String quantity, String url) {
        Fullname = fullname;
        Notes = notes;
        Price = price;
        Quantity = quantity;
        this.url = url;
    }
    public Item() {
    }
    public long getId(){return Id;}
    public void setId(long id){Id=id;}

    public void setFullname(String fullname) {

        Fullname = fullname;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFullname() {
        return Fullname;
    }

    public String getNotes() {
        return Notes;
    }

    public String getPrice() {
        return Price;
    }

    public String getQuantity() {
        return Quantity;
    }

    public String getUrl() {
        return url;
    }
}
