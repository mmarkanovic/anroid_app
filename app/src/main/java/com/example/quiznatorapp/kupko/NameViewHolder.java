package com.example.quiznatorapp.kupko;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private TextView tvName;
    private NameClickListener clickListener;
    private Button btnAdd;
    private ImageView itemImage;

    public NameViewHolder(@NonNull View itemView, NameClickListener listener) {
        super(itemView);
        this.clickListener = listener;
        tvName=itemView.findViewById(R.id.tvName);
        btnAdd= itemView.findViewById(R.id.btnAdd);
        itemImage=itemView.findViewById(R.id.itemImage);
        btnAdd.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                clickListener.onNameClick(getAdapterPosition( ));
            }
        });
    }

    public void setName(String name,String url){
        if(itemView.getContext().toString().contains("CartActivity")){
      btnAdd.setText("Ukloni");
    }
        tvName.setText(name);
        Picasso.get().load(url).into(itemImage);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==btnAdd.getId())
        clickListener.onNameClick(getAdapterPosition());
    }
}
