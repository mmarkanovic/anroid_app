package com.example.quiznatorapp.kupko;


import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ShippingInfo extends RealmObject implements Serializable {
    @PrimaryKey
    private long Id;
    String Name;
    String Surname;
    String Adress;
    String Phone;


        public ShippingInfo(String name, String surname, String adress, String phone,long id) {
        Id=id;
        Name = name;
        Surname = surname;
        Adress = adress;
        Phone = phone;
    }

    public ShippingInfo() {
    }
    public void setName(String name) {
        Name = name;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public void setAdress(String adress) {
        Adress = adress;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public String getAdress() {
        return Adress;
    }

    public String getPhone() {
        return Phone;
    }
    public long getId(){return Id;}

}
