package com.example.quiznatorapp.kupko;

import java.io.Serializable;
import java.util.List;

public class order implements Serializable {
    private List<Item> items;
    private ShippingInfo shippingInfo;

    public void setItems(List <Item> items) {
        this.items = items;
    }

    public order(List <Item> items, ShippingInfo shippingInfo) {
        this.items = items;
        this.shippingInfo = shippingInfo;
    }

    public List <Item> getItems() {

        return items;

    }

}
