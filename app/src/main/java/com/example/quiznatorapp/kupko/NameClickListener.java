package com.example.quiznatorapp.kupko;

public interface NameClickListener {
    void onNameClick(int position);
}
