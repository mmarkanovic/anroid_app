package com.example.quiznatorapp.kupko;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CartActivity  extends AppCompatActivity implements NameClickListener{
    private List<Item> CartItems;

    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;
    Button shipping;
    Button cart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        //setupRecycler();
        CartItems=new ArrayList <Item>();
        Intent i=getIntent();
        for (Item item : CartItems = (List <Item>) i.getSerializableExtra("Cart")) {

        }
        ;

            mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mViewPager = findViewById(R.id.container);
            setupViewPager(mViewPager);

            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);



    }


    @Override
    public void onNameClick(int position) {
      //  recyclerAdapterCart.removeCell(position);
        CartItems.remove(position);
    }
    private void setupViewPager(ViewPager viewPager) {

        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new ShippingLayout(), "Podaci za dostavu");
        adapter.addFragment(new CartFragment(), "Košarica");
        viewPager.setAdapter(adapter);

    }
}
